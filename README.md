# Springload Form

## Goals

1. Setup tests for TDD - DONE
2. Basic form setup with Material UI
   - Email (valid address) DONE
   - Password (longer than 8 characters) DONE
   - Favourite colour (only select one) DONE
   - Favourite animals (multichoice) DONE
   - If Tiger selected then show 'Tiger type' input and this is a required field DONE
3. Accessibility - test with keyboard navigation DONE
4. Test on BrowserStack for browser support NOT COMPLETED
5. Documentation - add to readme DONE
6. Progressive enhancement - went straight in with Material-ui for styling and design support, as well as accessibility support

## Notes

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). I chose this as it is a fast way to setup a simple app and comes with testing support.

I used [Material-UI](https://mui.com/material-ui/) components to help with fast, good UI, and to assist with validation.

To see the output of the form submission, open DevTools in the browser to see the log.

Given more time I would have tested on different browsers. I would collate the saved data into a more api friendly format. I would have liked to format the 'Tiger type' input field for better readability. I would have a reset button to reset the fields.

## Available Scripts

To run the app:

### `npm start`

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

To run the tests:

### `npm test`

### `yarn test`
