import * as React from 'react';
import FormLabel from '@mui/material/FormLabel';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

// Collect multichoice options of favourite animals

export default function AnimalsCheckboxes({updateAnimals}) {
  const [state, setState] = React.useState({
    bear: false,
    tiger: false,
    snake: false,
    donkey: false,
    tigerType: ''
  });

  const [error, setError] = React.useState(false)

  const handleChange = (event) => {
    setError(false)

    // If it is a checkbox set the animal as checked (true) otherwise
    // if Tiger type, store the string
    setState({
      ...state,
      [event.target.name]: event.target.name === 'tigerType' ? event.target.value : event.target.checked,
    });
  };

  const handleSave = () => {
    setError(false)
 
    // If tiger is checked then user must input a tiger type
    if (state.tiger && !state.tigerType) {
      setError(true)
    } else { updateAnimals(state) }
  }
  
return (
      <div data-testid="form--checkboxes" className="form-section">
        <FormLabel>Choose your favourite animals</FormLabel>
        <FormControlLabel
            control={
            <Checkbox checked={state.bear} onChange={handleChange} name="bear" />
            }
            label="Bear"
        />
        <FormControlLabel
            control={
            <Checkbox checked={state.tiger} onChange={handleChange} name="tiger" />
            }
            label="Tiger"
        />
        {state.tiger && 
            <FormControlLabel
                control={
                    <TextField
                        required
                        id="textfield-required"
                        name="tigerType"
                        label="Required"
                        value={state.tigerType}
                        error={error}
                        helperText="This field is required"
                        onChange={(event) => handleChange(event)}
                  />
              }
              label="Enter favourite Tiger type"
    
            />}
        <FormControlLabel
            control={
              <Checkbox checked={state.snake} onChange={handleChange} name="snake" />
            }
            label="Snake"
        />
        <FormControlLabel
            control={
              <Checkbox checked={state.donkey} onChange={handleChange} name="donkey" />
            }
            label="Donkey"
    />
      
    <div className="buttonWrapper"> <Button data-testid="button--save" size="medium" variant="outlined" onClick={handleSave} disabled={error}>Save</Button></div>

    </div>
  )
}