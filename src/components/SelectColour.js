import * as React from 'react';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormLabel from '@mui/material/FormLabel';

// Store a single colour

export default function SelectColour({colour, updateColour}) {

  return (
    <div data-testid="form--select" className="form-section">
      <FormControl sx={{ m: 1, minWidth: 120 }} className="form-control">
          <FormLabel>Choose your favourite colour</FormLabel>

          <Select
            value={colour}
            label="Colour"
            onChange={event => updateColour(event.target.value)}
          >
              <MenuItem value="blue">Blue</MenuItem>
              <MenuItem value="green">Green</MenuItem>
              <MenuItem value="red">Red</MenuItem>
              <MenuItem value="black">Black</MenuItem>
          </Select>
      </FormControl>
    </div>
  )
}