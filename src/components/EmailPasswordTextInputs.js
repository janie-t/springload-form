import * as React from 'react';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

// Collect the user Email and Password

export default function EmailPasswordTextInput({updateEmailPassword}) {

    const [state, setState] = React.useState({
        email: '',
        password: ''
    });

    const [emailError, setEmailError] = React.useState(false)
    const [passwordError, setPasswordError] = React.useState(false)

    const isValidEmail = (email) => {
        return /\S+@\S+\.\S+/.test(email);
    }
    
    const handleChange = (name, value) => {
        setState({
        ...state,
        [name]: value,
        });
    }

    const handleSave = () => {
        setEmailError(false)
        setPasswordError(false)
 
        // Must input a valid email format
        if (!isValidEmail(state.email)) {
            setEmailError(true)
        }

        // Must input a password at least 8 characters long
        if (state.password.length < 9) {
            setPasswordError(true)
        }
 
        if (state.email && state.password) {
            // If both are valid, send the state up to the parent component
            updateEmailPassword(state)
        }
        updateEmailPassword(state)
    }
    
    return (
        <div data-testid="form--text-inputs" className="form-section">
            <FormLabel>Enter your email and password</FormLabel>

            <FormControl >
                <div className="text-input">
                    <TextField type="email" color='primary' variant='outlined' label="Email" placeholder="Email address" value={state.email} onChange={event => handleChange('email', event.target.value)}  error={emailError}  helperText="Not a valid email address"
/>
                </div>
            </FormControl>
            <FormControl>
                <div className="text-input">
                    <TextField type="text" color='primary' variant='outlined' label="Password" placeholder="Password" value={state.password} onChange={event => handleChange('password', event.target.value)} error={passwordError} helperText="Password must be at least 8 characters"/>
                </div>
            </FormControl>
           <div className="buttonWrapper"> <Button data-testid="button--save" size="medium" variant="outlined" onClick={handleSave} disabled={!state.email || !state.password}>Save</Button></div>
        </div>
  )
}
