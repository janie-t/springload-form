/* eslint-disable jest/valid-expect */
import { render, screen } from '@testing-library/react';
import App from './App.js';

test('renders page', () => {
  render(<App />);
  expect(screen.getByTestId('page-title'))
});

test('renders form', () => {
  render(<App />);
  expect(screen.getByTestId('form'))
});

test('renders text inputs', () => {
  render(<App />);
  expect(screen.getByTestId('form--text-inputs'))
});

test('renders select', () => {
  render(<App />);
  expect(screen.getByTestId('form--select'))
});

test('renders checkboxes', () => {
  render(<App />);
  expect(screen.getByTestId('form--checkboxes'))
});

test('renders submit button', () => {
  render(<App />);
  expect(screen.getByTestId('button--submit'))
});