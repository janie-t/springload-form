import React from 'react'
import Button from '@mui/material/Button';
import AnimalsCheckboxes from './components/AnimalsCheckboxes'
import EmailPasswordTextInputs from './components/EmailPasswordTextInputs'
import SelectColour from './components/SelectColour'

const Form = () => {
    
    // Store any saved email and password data here
    const [emailPassword, setEmailPassword] = React.useState({})

    // Store colour here
    const [colour, setColour] = React.useState('')

    // Store animals here
    const [animals, setAnimals] = React.useState({})
 
    const handleSubmit = (event) => {
        event.preventDefault()
        console.log('email/password', emailPassword, 'colour', colour, 'animals', animals)
    }

    return (
        <form data-testid="form" onSubmit={handleSubmit}>
            <EmailPasswordTextInputs updateEmailPassword={newState => setEmailPassword(newState)}/><br/>
            
            <SelectColour colour={colour} updateColour={colour => setColour(colour)} /><br/>

            <AnimalsCheckboxes updateAnimals={animals => setAnimals(animals)} /><br />

            <div className="buttonSubmit"><Button data-testid="button--submit" variant="outlined" fullWidth onClick={handleSubmit}>Submit</Button></div>
        </form>
  )
}

export default Form