import './App.css';
import Form from './Form.js'

function App() {
  return (
    <div className="App" >
      <h1 data-testid="page-title">Contact form</h1>
      <Form />
    </div>
  );
}

export default App;
